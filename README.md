in CODE directory:

Run file L2.py to produce error norm of all advection schemes for part 2 questions 3.

Run file stabilityExpt.py to produce graph for part 2 question 4.

Initial conditions can be found in file InitialConditions.py, and the schemes are defined in file advectionSchemesAll.py, error norm are defined in errorNormSchemes.py

