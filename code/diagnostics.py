# -*- coding: utf-8 -*-
"""
Created on Tue Nov 20 14:47:21 2018

@author: dk802106
"""
from __future__ import absolute_import, division, print_function
import numpy as np

def L2ErrorNorm(phi, phiExact):
    "Calculates the L2 error norm (RMS error) of phi in comparison to"
    "phiExact"
    'written by 26802106'
    phiError = phi - phiExact
    L2 = np.sqrt(sum(phiError**2)/sum(phiExact**2))

    return L2
    


    