'''
All advection schemes implemented by group
both for question 1 (FTBS, CTCS and CNCS)
and additional schemes for question 2 (Lax-Wendroff, Warming and Beam,
Semi-Lagrangian)
'''

# Imports
from __future__ import absolute_import, division, print_function
import scipy.linalg as la
import numpy as np


def FTBS(phiOld, c, nt):
    'Diffusion of profile in phiOld using FTBS using non-demensional advection\
coefficient, c, written by 27800003'
    nx = len(phiOld)

    #new time-step array for phi
    phi = phiOld.copy()
    
    #FTBS for all time steps
    for it in xrange(nt):

    #apply FTBS formula for each point in space j
        for j in xrange(nx):
            phi[j] = phiOld[j] - (c* (phiOld[j] - phiOld[(j-1)%nx]))

        #for the next timestep copy current phi into old
        phiOld = phi.copy()

    return phi


def CTCS(phiOld, c, nt):
    "written by 26822470"
    "Adection of initial conditions in phiOld using CTCS using "
    "Courant number, c"

    nx = len(phiOld)

    # middle time-step array for phi
    phi = phiOld.copy()
    
    # do FTCS for first timestep, since do not have any previous time values
    for it in xrange(0):
        
        for j in xrange(nx):
                phi[j] = phiOld[j] - c/2*(phiOld[(j+1)%nx] - phiOld[(j-1)%nx])
        
        # newest time step array for phi
    phiNew = phi.copy()

    # CTCS for all time steps apart from first
    for it in xrange(1,nt): # make sure nt is int

        # Code for CTCS at each time-step
        for j in xrange(nx):
            phiNew[j] = phiOld[j] - c*(phi[(j+1)%nx] - phi[(j-1)%nx])
        
        # update phiOld to be sequal to last caclulated phi
        phiOld = phi.copy()
        phi = phiNew.copy()

    return phi


#function of CNCS scheme
def CNCS(phi,c,nt):
    'writen by 26802106'
    'advection of profile in phiOld using CNCS '
    "courant number, c for nt time steps with periodic boundary conditions"

    nx=len(phi)
    #array representing CNCS
    M1=np.zeros([nx,nx])
    M2=np.zeros([nx,nx])
    for i in xrange(nx):
        M1[i,(i-1)%nx]=-c/4.    #periodic boundary conditions
        M1[i,i]=1
        M1[i,(i+1)%nx]=c/4.
                                                                                  
        M2[i,(i-1)%nx]=c/4.
        M2[i,i]=1
        M2[i,(i+1)%nx]=-c/4.
    # CNCS for all time steps
    for it in xrange(nt):
        phi=la.solve(M1,np.dot(M2,phi))
    return phi

    
def lax(phiOld,c,nt):
    'writen by 26802106'
    'advection of profile in phiOld using lax-wendroff '
    "courant number, c for nt time steps with periodic boundary conditions"

    nx=len(phiOld)
    #new time-step array for phi
    phi = phiOld.copy()
    # lax-wendroff for rest of time steps
    for it in xrange(int(nt)):
        for j in xrange(int(nx)):
           phi[j]=c*(c-1)/2.*phiOld[(j+1)%nx]+(1-c*c)*phiOld[j]+c*(1+c)/2.*phiOld[(j-1)%nx]
        phiOld = phi.copy()
    return phi     


def WarmingBeam(phiOld, c, nt):
    'written by 27800003'
    'Advection of initial conditions in phiOld using Warming and Bean with\
    courant number, c'
    nx = len(phiOld)
    
    #new time-step array for phi
    phi = phiOld.copy()
    
    #Warming and Bean for all time steps
    for it in xrange(nt):
        #apply Warming and Bean formula for each point in space j
        for j in xrange(nx):
            phi[j] = -c*( (0.5*(3-c)*phiOld[j] - 0.5*(1-c)*phiOld[j-1])\
                          -(0.5*(3-c)*phiOld[j-1] - 0.5*(1-c)*phiOld[j-2])) \
                          + phiOld[j]
        
        #for the next timestep copy current phi into old
        phiOld = phi.copy()

    return phi

def semiLag(phiOld, c, nt):
    "written by 26822470"
    "Adection of initial conditions in phiOld using semi-Lagrangian method and"
    "Courant number, c. Uses cubic Lagrange method of interpolation to find"
    "new phi from upstream"

    nx = len(phiOld)
    
    # new timestep array for phi
    phi = phiOld.copy()

    for it in xrange(nt):
        
        for j in xrange(nx):
            k = int(np.floor(j-c)) # find closest integer below upstream point
            B = j - k - c # beta for interpolation
            
            # find phi at new time using cubic Lagrange interpolation
            phi[j] = -1/6*B*(1-B)*(2-B)*phiOld[(k-1)%nx] \
            + 1/2*(1+B)*(1-B)*(2-B)*phiOld[k] \
            + 1/2*(1+B)*B*(2-B)*phiOld[(k+1)%nx] \
            - 1/6*(1+B)*B*(1-B)*phiOld[(k+2)%nx]
            
        # update phiOld to current phi
        phiOld = phi.copy()

    return phi

