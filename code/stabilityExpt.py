# -*- coding: utf-8 -*-
"""
Created on Sun Dec 02 11:08:50 2018

@author: dk802106
"""

from __future__ import absolute_import, division, print_function

import matplotlib.pyplot as plt
import numpy as np

# read in all the linear advection schemes, initial conditions and other
# code associated with this application

exec(open("InitialConditions.py").read())
exec(open("advectionSchemesAll.py").read())
from advectionSchemesAll import FTBS,CTCS,CNCS,lax,WarmingBeam,semiLag

def plotting(phiOld, IC,T,dx,x,u):
    'written by 26802106'
    "function to plot serror norm of various numerical schemes"
    "(FTCS, CTCS,  Lax-Wendroff)"
    " uses Initial conditions, IC, can equal 1 (square wave) or 2 (cosine)"
    " uses x (spatial points), c (Courant number), nt (no. of time steps)"
    #parameters
    nt = 50
    
    # for time-step
    if nt > 0:
        dt = T/nt
    elif nt == 0:
        dt = 0
    else:
        raise Exception("Cannot have a time-step less than zero.")
    # Courant number
    c = dt*u/dx 
    
    # advection using various numerical schemes
    phiFTBS = FTBS(phiOld.copy(), c, nt)
    phiCTCS = CTCS(phiOld.copy(), c, nt)        
    phiCNCS = CNCS(phiOld.copy(), c, nt)
    phiLW = lax(phiOld.copy(), c, nt)
    phiWB = WarmingBeam(phiOld.copy(), c, nt)
    phisemiLag = semiLag(phiOld.copy(), c, nt)

    
   #Plot the solutions
    
    plt.figure(figsize=(12,5))
    plt.subplot(1,2,1)
    plt.plot(x,phiFTBS, label='FTBS',color='green')
    plt.plot(x,phiCTCS, label='CTCS',color='blue')
    plt.plot(x,phiCNCS, label='CNCS',color='red')
    plt.plot(x, phiLW, label='Lax-Wendroff',color='orange')
    plt.plot(x,phiWB, label='Warming and Beam',color='purple')
    plt.plot(x,phisemiLag, label='Semi Lagrangian',color='black')
    plt.ylim([0,1.1])
    plt.legend()
    plt.xlabel("distance")
    plt.ylabel("$\phi$")
    plt.title('numerical solutions  with c='+str(c))
        
    return
    
    
def main():
    'written by 26802106'
    "function to call on multiple numerical schemes to solve linear advection"
    "equation using given parameters"
    from InitialConditions import f_init1,f_init2
    #parameters
    xmin = 0.
    xmax = 1.
    nx=40
    dx = (xmax - xmin)/(nx) # spatial step
    # spatial points for plotting and for defining initial conditions
    x = np.zeros(nx)
    for j in xrange(nx):
        x[j] = xmin + j*dx
    print('x=',x)
  
    # cosine wave initial conditions with different T, which cause different c
    
    plotting(f_init2(x), 2,0.125,dx,x,1)
    plotting(f_init2(x), 2,1.875,dx,x,1)
    plotting(f_init2(x), 2,3.125,dx,x,1)
    plotting(f_init1(x), 2,0.125,dx,x,1)
    plotting(f_init1(x), 2,1.875,dx,x,1)
    plotting(f_init1(x), 2,3.125,dx,x,1)
    
    return
    
main()