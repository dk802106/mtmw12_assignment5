# -*- coding: utf-8 -*-
"""
Created on Sat Nov 24 10:10:51 2018

@author: dk802106
"""

from __future__ import absolute_import, division, print_function

import matplotlib.pyplot as plt
import numpy as np

# read in all the linear advection schemes, initial conditions and other
# code associated with this application

exec(open("InitialConditions.py").read())
exec(open("diagnostics.py").read())
exec(open("errorNormSchemes.py").read())
from errorNormSchemes import FTBS,CTCS,lax

def plot(T,nt,x,u,xmax,xmin):
    'written by 26802106'
    "function to plot serror norm of various numerical schemes"
    "(FTCS, CTCS,  Lax-Wendroff)"
    " uses Initial conditions, IC, can equal 1 (square wave) or 2 (cosine)"
    " uses x (spatial points), c (Courant number), nt (no. of time steps)"

    from InitialConditions import f_init1,f_init2
    dt = T/nt  #time steps
    c = dt*u/0.025 #Courant number
    t=np.zeros(nt)
    for j in xrange(nt):
        t[j] = 0.+ j*dt 
        
    #error norm using different schemes and different initial conditions
    phiOld=f_init1(x)  
    l2FTBS_S = FTBS(phiOld.copy(), c, nt,x,u,dt,xmax,xmin,1)
    l2CTCS_S = CTCS(phiOld.copy(), c, nt,x,u,dt,xmax,xmin,1)
    l2lax_S = lax(phiOld.copy(), c, nt,x,u,dt,xmax,xmin,1)
    
    phiOld=f_init2(x)  
    l2FTBS_C = FTBS(phiOld.copy(), c, nt,x,u,dt,xmax,xmin,2)
    l2CTCS_C = CTCS(phiOld.copy(), c, nt,x,u,dt,xmax,xmin,2)
    l2lax_C = lax(phiOld.copy(), c, nt,x,u,dt,xmax,xmin,2)
    
     #Plot the solutions
    plt.figure(figsize=(12,5))
    plt.subplot(1,2,1)
    plt.plot(t, l2FTBS_S, label='FTBS')
    plt.plot(t, l2CTCS_S, label='CTCS')
    plt.plot(t,l2lax_S,label='lax-wendroff')
    plt.ylim([0,1.1])
    plt.legend()
    plt.xlabel("time")
    plt.ylabel("$\l2$")
    plt.title('Square wave c='+str(c)+'_T='+str(T)+'_u='+str(u))
        
     #Plot the solutions
    plt.figure(figsize=(12,5))
    plt.subplot(1,2,2)
    plt.plot(t, l2FTBS_C, label='FTBS')
    plt.plot(t, l2CTCS_C, label='CTCS')
    plt.plot(t,l2lax_C,label='lax-wendroff')
    plt.ylim([0,1.1])
    plt.legend()
    plt.xlabel("time")
    plt.ylabel("$\l2$")
    plt.title(' Cosin wave c='+str(c)+'_T='+str(T)+'_u='+str(u))
    
    return
    
    
def main():
    'written by 26802106'
    "function to call on multiple numerical schemes to solve linear advection"
    "equation using given parameters"
    
    #Parameters
    xmin = 0.
    xmax = 1.
    nx = 40
    
    #Derived parameters
    dx = (xmax - xmin)/(nx) #spatial step
    
    #spatial points for plotting and for defining initial conditions
    x = np.zeros(nx)
    for j in xrange(nx):
        x[j] = xmin + j*dx
    print('x=',x)
    
   
    # plot solutions
    plot(30,1280,x,1.,xmax,xmin)
    return
    
main()