import numpy as np

# Initial conditions function for advection

def f_init1(x):
    "WRITE DOC STRING 27800003"
    phi = np.zeros_like(x)
    
    for j in xrange(len(x)):
        if x[j] >= 0 and x[j] < 0.5:
            phi[j] = 1.
        elif x[j] >= 0.5 and x[j] < 1:
            phi[j] = 0.

    return phi

def f_init2(x):
    "WRITE DOC STRING 27800003"

    phi = np.zeros_like(x)
    
    for j in xrange(len(x)):
        if x[j] >= 0 and x[j] < 0.5:
            phi[j] = 0.5 * (1 - np.cos(4* np.pi * x[j]))
        elif x[j] >= 0.5 and x[j] < 1:
            phi[j] = 0.

    return phi
