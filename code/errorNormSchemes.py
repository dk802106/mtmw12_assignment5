# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
exec(open("diagnostics.py").read())
from diagnostics import L2ErrorNorm
exec(open("InitialConditions.py").read())
from InitialConditions import f_init1,  f_init2
import numpy as np
def FTBS(phiOld, c, nt,x,u,dt,xmax,xmin,IC):
    'Diffusion of profile in phiOld using FTBS using non-demensional advection\
coefficient, c, written by 27800003, modified by 26802106'
    nx = len(phiOld)
    l2=np.zeros(nt)
    #new time-step array for phi
    phi = phiOld.copy()
    
    #FTBS for all time steps
    for it in xrange(nt):
        #calculate error norm for every time steps
        if IC==1:
            l2[it]=L2ErrorNorm(phi,f_init1((x - u*dt*it)%(xmax-xmin)))
        elif IC==2:
            l2[it]=L2ErrorNorm(phi,f_init2((x - u*dt*it)%(xmax-xmin)))
    #apply FTBS formula for each point in space j
        for j in xrange(nx):
            phi[j] = phiOld[j] - (c* (phiOld[j] - phiOld[(j-1)%nx]))
            #for the next timestep copy current phi into old
        
        phiOld = phi.copy()
    return l2


def CTCS(phiOld, c, nt,x,u,dt,xmax,xmin,IC):
    "written by 26822470, modified by 26802106"
    "Adection of initial conditions in phiOld using CTCS using "
    "Courant number, c"

    nx = len(phiOld)
    l2=np.zeros(nt)
    # middle time-step array for phi
    phi = phiOld.copy()
    
    # do FTCS for first timestep, since do not have any previous time values
    for it in xrange(int(nt)): # make sure nt is int
           #calculate error norm for every time steps
           if IC==1:
            l2[it]=L2ErrorNorm(phi,f_init1((x - u*dt*it)%(xmax-xmin)))
           elif IC==2:
            l2[it]=L2ErrorNorm(phi,f_init2((x - u*dt*it)%(xmax-xmin)))
    # Code for CTCS at each time-step
           phiNew = phi.copy()
           for j in xrange(nx):
                phiNew[j] = phiOld[j] - c*(phi[(j+1)%nx] - phi[(j-1)%nx])
           # update phiOld to be sequal to last caclulated phi
           phiOld = phi.copy()
           phi = phiNew.copy()

    return l2


def lax(phiOld,c, nt,x,u,dt,xmax,xmin,IC):
    'writen by 26802106'
    'advection of profile in phiOld using lax-wendroff '
    "courant number, c for nt time steps with periodic boundary conditions"

    nx=len(phiOld)
    l2=np.zeros(nt)
    #new time-step array for phi
    phi = phiOld.copy()
    # lax-wendroff for rest of time steps
    for it in xrange(int(nt)):
        #calculate error norm for every time steps
        if IC==1:
            l2[it]=L2ErrorNorm(phi,f_init1((x - u*dt*it)%(xmax-xmin)))
        elif IC==2:
            l2[it]=L2ErrorNorm(phi,f_init2((x - u*dt*it)%(xmax-xmin)))
    # Code for lax-wendroff at each time-step
        for j in xrange(int(nx)):
           phi[j]=c*(c-1)/2.*phiOld[(j+1)%nx]+(1-c*c)*phiOld[j]+c*(1+c)/2.*phiOld[(j-1)%nx]
        
        phiOld = phi.copy()
    return l2    
